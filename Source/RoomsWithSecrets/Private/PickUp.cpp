// "Rooms With Secrets" by Natatem00. 2019


#include "PickUp.h"
#include "GameFramework/Character.h"
#include "Components/StaticMeshComponent.h"
#include "Components/AudioComponent.h"
#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
APickUp::APickUp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	mesh->SetSimulatePhysics(true);
	mesh->CanCharacterStepUpOn = ECB_No;
	mesh->SetMassOverrideInKg(NAME_None, 150.f);
	RootComponent = mesh;
	isHolding = false;
	useGravity = true;

	IInteractable::SetMesh(mesh);

	audioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	audioComponent->bAutoActivate = false;

	audioInteractable.SetAudioComponent(audioComponent);

	SetReplicates(true);
	SetReplicateMovement(true);
}

// Called when the game starts or when spawned
void APickUp::BeginPlay()
{
	Super::BeginPlay();
	SetInitPosition(GetActorLocation());
	SetInitRotation(GetActorRotation());

	mesh->bRenderCustomDepth = false;
	mesh->CustomDepthStencilValue = 0;
	mesh->SetCollisionObjectType(ECollisionChannel::ECC_PhysicsBody);
	mesh->OnComponentHit.AddDynamic(this, &APickUp::OnCompHit);

	mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Block);
	mesh->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel1);
}

void APickUp::OnCompHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit)
{
	audioInteractable.OnCompHit(HitComponent, OtherActor, OtherComponent, NormalImpulse, Hit);
}

// Called every frame
void APickUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (isHolding && holdingComponent)
	{
		SetActorLocationAndRotation(holdingComponent->GetComponentLocation(), holdingComponent->GetComponentRotation());
	}
}

void APickUp::Rotate(const FRotator controlRotator)
{
	SetActorRotation(FQuat(controlRotator));
}

void APickUp::Use(AActor* useObject)
{
	
}

void APickUp::PickUp(AActor* actorToPickup)
{
	TogglePickUp();

	if (!isHolding)
	{
		mesh->AddForce(Cast<UCameraComponent>(actorToPickup->GetComponentByClass(UCameraComponent::StaticClass()))->GetForwardVector() * 1000 * mesh->GetMass());
	}
	else
	{
		holdingComponent = Cast<USceneComponent>(actorToPickup->GetDefaultSubobjectByName(TEXT("HoldingComponent")));
	}
}

void APickUp::TogglePickUp()
{
	isHolding = !isHolding;
	useGravity = !useGravity;

	mesh->SetNotifyRigidBodyCollision(!isHolding);
	mesh->SetEnableGravity(useGravity);
	mesh->SetSimulatePhysics(!isHolding);
	mesh->SetCollisionEnabled(isHolding ? ECollisionEnabled::NoCollision : ECollisionEnabled::QueryAndPhysics);
	audioInteractable.SetColliding(!isHolding);
}

void APickUp::SetSoundCue(USoundCue * audioCue)
{
	audioComponent->SetSound(audioCue);
}


