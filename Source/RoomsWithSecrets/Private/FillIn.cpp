// "Rooms With Secrets" by Natatem00. 2019


#include "FillIn.h"

#include "Components/StaticMeshComponent.h"
#include "Components/AudioComponent.h"

// Sets default values
AFillIn::AFillIn()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

void AFillIn::Use(AActor* useObject)
{
	if (ready)
	{
		if (hitCue)
		{
			SetSoundCue(hitCue);
		}
	}
	if (useObject)
	{
		GetAudioComponent()->Play();
		soupMesh->SetActorHiddenInGame(false);
		soupMesh->AddActorLocalOffset(FVector(0, 0, 6));
		useObject->Destroy(true);

	}
}

// Called when the game starts or when spawned
void AFillIn::BeginPlay()
{
	Super::BeginPlay();
	if (soupMesh)
	{
		FAttachmentTransformRules rules(EAttachmentRule::KeepWorld, false);
		soupMesh->AttachToActor(this, rules);
		soupMesh->SetActorRelativeLocation(FVector(0, 0, 17));
		soupMesh->SetActorRelativeScale3D(FVector(1, 1, 1));
		soupMesh->SetActorHiddenInGame(true);
	}
}

// Called every frame
void AFillIn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

