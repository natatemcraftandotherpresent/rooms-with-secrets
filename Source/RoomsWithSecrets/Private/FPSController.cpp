// "Rooms With Secrets" by Natatem00. 2019


#include "FPSController.h"

#include "Components/InputComponent.h"
#include "Components/AudioComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#include "Camera/CameraShake.h"
#include "DrawDebugHelpers.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

#include "CEaseInOutInterpolation.h"
#include "FlashLight.h"
#include "FlashlightComponent.h"
#include "Interactable.h"

#define GETENUMSTRING(etype, evalue) ( (FindObject<UEnum>(ANY_PACKAGE, TEXT(etype), true) != nullptr) ? FindObject<UEnum>(ANY_PACKAGE, TEXT(etype), true)->GetEnumName((int32)evalue) : FString("Invalid - are you sure enum uses UENUM() macro?") )

// Sets default values
AFPSController::AFPSController() : flashlightOn(false), crouch(false)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	holdingComponent = CreateDefaultSubobject<USceneComponent>(TEXT("HoldingComponent"));

	isHolding = isInspecting = false;

	audioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("Take"));
	audioComponent->bAutoActivate = false;
}

// Called when the game starts or when spawned
void AFPSController::BeginPlay()
{
	Super::BeginPlay();
	pitchMax = GetWorld()->GetFirstPlayerController()->PlayerCameraManager->ViewPitchMax;
	pitchMin = GetWorld()->GetFirstPlayerController()->PlayerCameraManager->ViewPitchMin;
}

void AFPSController::SetCanMove(bool move)
{
	if (!move)
	{
		lastRotation = GetControlRotation();
	}
	else
	{
		GetController()->SetControlRotation(lastRotation);
	}
	springArm->bUsePawnControlRotation = move;
	bUseControllerRotationYaw = move;
	springArm->SetWorldRotation(GetControlRotation());
	canMove = move;
}

// Called every frame
void AFPSController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Crouch(DeltaTime);

	// check for interactable objects
	CheckInteractableObject();
	
	InspectObject(DeltaTime);

	ToggleFlashlight(DeltaTime);


	CheckStep();

	if (!currentPickupObj && isHolding)
	{
		isHolding = false;
	}
	//UE_LOG(LogTemp, Warning, TEXT("%s"), *GETENUMSTRING("ENetRole", Role));
}


void AFPSController::ToggleFlashlight_Implementation(float DeltaTime)
{
	if (flashlightComponent && canMove)
	{
		if (flashlightOn && flashlightComponent->OnFlashlight(DeltaTime)) {}
		else if (!flashlightOn && flashlightComponent->OffFlashlight(DeltaTime)) {}
	}
}

bool AFPSController::ToggleFlashlight_Validate(float DeltaTime)
{
	return true;
}

void AFPSController::MoveRight(float value)
{
	if (!isInspecting && canMove)
	{
		const FRotator rotation = Controller->GetControlRotation();
		const FRotator yawRotation(0, rotation.Yaw, 0);

		const FVector direction = FRotationMatrix(yawRotation).GetUnitAxis(EAxis::Y);
		float movePower = moveSpeedFactor * value;
		AddMovementInput(direction, movePower);
		GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(cameraShake, movePower);
	}
}

void AFPSController::MoveForward(float value)
{
	if (!isInspecting && canMove)
	{
		const FRotator rotation = Controller->GetControlRotation();
		const FRotator yawRotation(0, rotation.Yaw, 0);

		const FVector direction = FRotationMatrix(yawRotation).GetUnitAxis(EAxis::X);
		float movePower = moveSpeedFactor * value;
		AddMovementInput(direction, movePower);
		GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(cameraShake, movePower);
	}
}

void AFPSController::Crouch(float DeltaTime)
{
	if (springArm && canMove)
	{
		FVector localTransform = GetTransform().InverseTransformPosition(springArm->GetComponentLocation());
		if (crouch && springArm && localTransform.Z > 22.f)
		{
			float zValue = easeInOutInterpolation.Interpolate(localTransform.Z, 15, 1.f, DeltaTime);
			springArm->SetRelativeLocation(FVector(0, 0, zValue));
		}
		else if (!crouch && springArm && localTransform.Z < 58.f)
		{
			auto zValue = easeInOutInterpolation.Interpolate(localTransform.Z, 60, 1.f, DeltaTime);
			springArm->SetRelativeLocation(FVector(0, 0, zValue));
		}
	}
}

void AFPSController::OnCrouch()
{
	if (canMove)
	{
		crouch = !crouch;
		moveSpeedFactor = crouch ? 0.3f : 0.7f;
		stepTime = crouch ? 0.5f : 0.4f;
		easeInOutInterpolation.Reset();
	}
}

void AFPSController::OnFlashlight()
{
	if (canMove)
	{
		flashlightOn = !flashlightOn;
		flashlightComponent->PlaySound(flashlightOn);
	}
}

void AFPSController::OnAction()
{
	if (canMove)
	{
		if (currentPickupObj && !isInspecting && !isHolding)
		{
			currentPickupObj->PickUp(this);
			GetWorld()->GetFirstPlayerController()->PlayDynamicForceFeedback(0.7f, 0.1f, false, true, false, true);
			ToggleItemPickup();
		}
		else if (currentPickupObj && isHolding)
		{
			if (Cast<IInteractable>(lastHitedObject))
			{
				GetWorld()->GetFirstPlayerController()->PlayDynamicForceFeedback(0.6f, 0.1f, false, true, false, true);
				currentPickupObj->Use(this);
			}
		}
	}
}

void AFPSController::OnInspect()
{
	if (isHolding && canMove)
	{
		ToggleMove();
	}
}

void AFPSController::OnInspectReleased()
{
	if (isInspecting && isHolding && canMove)
	{
		GetController()->SetControlRotation(lastRotation);
		GetWorld()->GetFirstPlayerController()->PlayerCameraManager->ViewPitchMax = pitchMax;
		GetWorld()->GetFirstPlayerController()->PlayerCameraManager->ViewPitchMin = pitchMin;
		ToggleMove();
	}
}

void AFPSController::OnCancel()
{
	if (canMove)
	{
		if (currentPickupObj && isHolding && !isInspecting)
		{
			GetWorld()->GetFirstPlayerController()->PlayDynamicForceFeedback(0.8f, 0.2f, false, true, false, true);
			currentPickupObj->PickUp(this);
			ToggleItemPickup();
		}
	}
}

void AFPSController::ToggleItemPickup()
{
	if (currentPickupObj && !isInspecting && canMove)
	{
		isHolding = !isHolding;
		if (!isHolding)
		{
			currentPickupObj = nullptr;
		}
		else
		{
			audioComponent->Play();
			currentPickupObj->Outline(false);
		}
	}
}

void AFPSController::ToggleMove()
{
	if (canMove)
	{
		lastRotation = GetControlRotation();
		isInspecting = !isInspecting;
		if (springArm)
		{
			springArm->bUsePawnControlRotation = !springArm->bUsePawnControlRotation;
			springArm->SetWorldRotation(GetControlRotation());
		}
		bUseControllerRotationYaw = !bUseControllerRotationYaw;
	}
}

void AFPSController::CheckInteractableObject()
{
	if (cameraComponent && canMove)
	{
		if (GetWorld()->LineTraceSingleByChannel(hitResult, cameraComponent->GetComponentLocation(), (cameraComponent->GetForwardVector() * 200.0f) + cameraComponent->GetComponentLocation(), ECollisionChannel::ECC_GameTraceChannel1))
		{
			if (Cast<IInteractable>(hitResult.GetActor()))
			{
				Cast<IInteractable>(hitResult.GetActor())->Outline(true);
				if (!isHolding)
				{
					if (Cast<IInteractable>(hitResult.GetActor()) != currentPickupObj)
					{
						if (currentPickupObj)
						{
							currentPickupObj->Outline(false);
						}
						currentPickupObj = Cast<IInteractable>(hitResult.GetActor());
					}
				}
				else
				{
					if (Cast<IInteractable>(hitResult.GetActor()) != lastHitedObject)
					{
						if (lastHitedObject)
						{
							lastHitedObject->Outline(false);
						}
						lastHitedObject = Cast<IInteractable>(hitResult.GetActor());
					}
				}
			}
			else
			{
				if (currentPickupObj)
				{
					currentPickupObj->Outline(false);
				}
				if (lastHitedObject)
				{
					lastHitedObject->Outline(false);
				}
			}
		}
		else
		{
			if (currentPickupObj)
			{
				currentPickupObj->Outline(false);
			}
			if (lastHitedObject)
			{
				lastHitedObject->Outline(false);
			}
		}
	}
}

void AFPSController::InspectObject(float DeltaTime)
{
	if (cameraComponent && canMove)
	{
		if (isInspecting && isHolding)
		{
			if (interpolFactor < 1)
			{
				interpolFactor += 2 * DeltaTime;
			}
			holdingComponent->SetRelativeLocation(FMath::Lerp(FVector(70, -40, -45), FVector(100, 0, 0), interpolFactor));
			GetWorld()->GetFirstPlayerController()->PlayerCameraManager->ViewPitchMax = 179.f;
			GetWorld()->GetFirstPlayerController()->PlayerCameraManager->ViewPitchMin = -179.f;
			currentPickupObj->Rotate(GetControlRotation());
		}
		else if (!isInspecting && isHolding && interpolFactor > 0)
		{
			interpolFactor -= 2 * DeltaTime;
			holdingComponent->SetRelativeLocation(FMath::Lerp(FVector(70, -40, -45), FVector(100, 0, 0), interpolFactor));
		}
	}
}

void AFPSController::YawRotation(float rotation)
{
	APawn::AddControllerYawInput(rotation * controlSettings.mouseSensivity * 10);
}

void AFPSController::PitchRotation(float rotation)
{
	APawn::AddControllerPitchInput(rotation * controlSettings.mouseSensivity * 10);
}

EPhysicalSurface AFPSController::CheckStep()
{
	FCollisionQueryParams QueryParams("Detection", false, this);
	QueryParams.bReturnPhysicalMaterial = true;
	if (GetWorld()->LineTraceSingleByChannel(hitResult, GetActorLocation(), GetActorLocation() - FVector(0, 0, 150), ECollisionChannel::ECC_Visibility, QueryParams))
	{
		return hitResult.PhysMaterial.Get()->SurfaceType.GetValue();
	}

	return EPhysicalSurface();
}

// Called to bind functionality to input
void AFPSController::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// move
	PlayerInputComponent->BindAxis("HorizontalMove", this, &AFPSController::MoveRight);
	PlayerInputComponent->BindAxis("VerticalMove", this, &AFPSController::MoveForward);

	// mouse
	PlayerInputComponent->BindAxis("MouseHorizontal", this, &AFPSController::YawRotation);
	PlayerInputComponent->BindAxis("MouseVertical", this, &AFPSController::PitchRotation);

	// actions
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &AFPSController::OnCrouch);
	PlayerInputComponent->BindAction("Flashlight", IE_Pressed, this, &AFPSController::OnFlashlight);
	PlayerInputComponent->BindAction("Action", IE_Pressed, this, &AFPSController::OnAction);
	PlayerInputComponent->BindAction("Inspect", IE_Pressed, this, &AFPSController::OnInspect);
	PlayerInputComponent->BindAction("Inspect", IE_Released, this, &AFPSController::OnInspectReleased);
	PlayerInputComponent->BindAction("Cancel", IE_Pressed, this, &AFPSController::OnCancel);

}

void AFPSController::PickUpItem(IInteractable* item)
{
	currentPickupObj = item;
	currentPickupObj->PickUp(this);
}

#pragma region Get/Set
void AFPSController::SetSpringArm(USpringArmComponent* spring) 
{ 
	springArm = spring; 
	springOriginalPosition = GetTransform().InverseTransformPosition(springArm->GetComponentLocation());
	crouchPosition = springOriginalPosition - FVector(0, 0, 30);
}
void AFPSController::SetCamera(UCameraComponent * camera)
{
	cameraComponent = camera;
	if (holdingComponent)
	{
		holdingComponent->AttachTo(cameraComponent);
		holdingComponent->SetRelativeLocation(FVector(70, -40, -45));
	}
}

void AFPSController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AFPSController, flashlightComponent);
}

#pragma endregion

