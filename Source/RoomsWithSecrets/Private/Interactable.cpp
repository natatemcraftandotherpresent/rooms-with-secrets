// "Rooms With Secrets" by Natatem00. 2019


#include "Interactable.h"

// Add default functionality here for any IInteractable functions that are not pure virtual.

void IInteractable::SetInitPosition(FVector position)
{
	initPosition = position;
}

FVector IInteractable::GetInitPosiiton() const
{
	return initPosition;
}

void IInteractable::SetInitRotation(FRotator rotation)
{
	initRotation = rotation;
}

FRotator IInteractable::GetInitRotation() const
{
	return initRotation;
}

void IInteractable::SetMesh(UStaticMeshComponent * mesh)
{
	changeMesh.SetMesh(mesh);
}

void IInteractable::Outline(bool show)
{
	changeMesh.Outline(show);
}
