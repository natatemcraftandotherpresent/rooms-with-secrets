// Fill out your copyright notice in the Description page of Project Settings.


#include "CEaseInOutInterpolation.h"

//
//bool CEaseInOutInterpolation::Setup(float from, float to, float time)
//{
//	if (time > 0)
//	{
//		value = from;
//		target = to;
//		speed = 0.0f;
//		acceleration = (to - from) / (time * time * 0.25f);
//		remainingTime = totalTime = time;
//		return true;
//	}
//	return false;
//}
//
//float CEaseInOutInterpolation::Interpolate(float deltaTime)
//{
//	if (remainingTime > 0)
//	{
//		remainingTime -= deltaTime;
//		if (remainingTime < totalTime * 0.5f)
//		{
//			speed -= acceleration * deltaTime;
//		}
//		else
//		{
//			speed += acceleration * deltaTime;
//		}
//		value += speed * deltaTime;
//	}
//	return value;
//}

float CEaseInOutInterpolation::Interpolate(float from, float to, float time, float deltaTime)
{
	if (acceleration == 0)
	{
		value = from;
		speed = 0.0f;
		acceleration = (to - from) / (time * time * 0.25f);
		remainingTime = time;
		totalTime = time;
	}
	if (remainingTime > 0)
	{
		remainingTime -= deltaTime;
		if (remainingTime < totalTime * 0.5f)
		{
			speed -= acceleration * deltaTime;
		}
		else
		{
			speed += acceleration * deltaTime;
		}
		value += speed * deltaTime;
		return value;
	}
	return to;
}

void CEaseInOutInterpolation::Reset()
{
	value = 0;
	speed = 0.0f;
	remainingTime = 0;
	totalTime = 0;
	acceleration = 0;
}
