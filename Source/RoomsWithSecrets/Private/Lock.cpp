// "Rooms With Secrets" by Natatem00. 2019


#include "Lock.h"

#include "Components/StaticMeshComponent.h"
#include "Components/AudioComponent.h"

#include "Chest.h"

// Sets default values
ALock::ALock()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	audioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));

	audioInteractable.SetAudioComponent(audioComponent);
	audioInteractable.SetColliding(false);
	//audioComponent->AttachTo(mesh);
}

void ALock::Rotate(const FRotator controllRotation)
{
}

void ALock::Use(AActor* useObject)
{
	//if (keyToOpen == *static_cast<FString*>(data) && mesh && audioComponent)
	//{
	//	lockMesh->AddRelativeLocation(FVector(0, -80, 0));
	//	keyToOpen = " ";
	//	mesh->SetSimulatePhysics(true);
	//	mesh->SetEnableGravity(true);
	//	audioInteractable.SetColliding(true);
	//	if (chest)
	//	{
	//		Cast<AChest>(chest)->Open(true);
	//	}
	//	IInteractable::SetMesh(nullptr);
	//	audioComponent->SetIntParameter("Lock", 1);
	//	audioComponent->Play();
	//}
	//else
	//{
	//	if (audioComponent && !audioComponent->IsPlaying())
	//	{
	//		audioComponent->SetIntParameter("Lock", 0);
	//		audioComponent->Play();
	//	}
	//}
	//return nullptr;
}

void ALock::PickUp(AActor* actorToPickup)
{

}

// Called when the game starts or when spawned
void ALock::BeginPlay()
{
	Super::BeginPlay();
	if (mesh)
	{
		IInteractable::SetMesh(mesh);
		mesh->SetNotifyRigidBodyCollision(true);
		mesh->BodyInstance.SetCollisionProfileName("BlockAllDynamic");
		mesh->OnComponentHit.AddDynamic(this, &ALock::OnCompHit);

		RootComponent = mesh;
	}

	audioComponent->bAutoActivate = false;
}

void ALock::OnCompHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit)
{
	if (!audioComponent->IsPlaying())
	{
		audioComponent->SetSound(hitCue);
		audioInteractable.OnCompHit(HitComponent, OtherActor, OtherComponent, NormalImpulse, Hit);
	}
}


