// Fill out your copyright notice in the Description page of Project Settings.


#include "CLinearInterpolation.h"


float CLinearInterpolation::Interpolate(float from, float to, float time, float deltaTime)
{
	if (step == 0)
	{
		remainingTime = time;
		value = from;
		step = (to - from) / time;
	}
	if (remainingTime > 0)
	{
		remainingTime -= deltaTime;
		value += step * deltaTime;
		return value;
	}
	return to;
}

void CLinearInterpolation::Reset()
{
	step = 0;
}
