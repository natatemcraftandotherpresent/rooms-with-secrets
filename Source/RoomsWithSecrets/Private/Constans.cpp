// Fill out your copyright notice in the Description page of Project Settings.


#include "Constans.h"

#include "Components/StaticMeshComponent.h"
#include "Components/AudioComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"


void ChangeMesh::Outline(bool show)
{
	if (meshComponent)
	{
		meshComponent->SetRenderCustomDepth(show);
		meshComponent->SetCustomDepthStencilValue(show ? 2 : 0);
	}
}

void AudioInteractable::SetAudioComponent(UAudioComponent * audioComponent)
{
	this->audioComponent = audioComponent;

}

void AudioInteractable::OnCompHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit)
{
	if (colliding)
	{
		if (Hit.PhysMaterial.Get())
		{
			switch (Hit.PhysMaterial.Get()->SurfaceType.GetValue())
			{
			case EPhysicalSurface::SurfaceType1:
				audioComponent->SetIntParameter("Material", 0);
				audioComponent->Play();
				break;
			case EPhysicalSurface::SurfaceType2:
				audioComponent->SetIntParameter("Material", 1);
				audioComponent->Play();
				break;
			case EPhysicalSurface::SurfaceType3:
				audioComponent->SetIntParameter("Material", 2);
				audioComponent->Play();
				break;
			case EPhysicalSurface::SurfaceType4:
				audioComponent->SetIntParameter("Material", 3);
				audioComponent->Play();
				break;
			case EPhysicalSurface::SurfaceType5:
				audioComponent->SetIntParameter("Material", 4);
				audioComponent->Play();
				break;
			case EPhysicalSurface::SurfaceType6:
				audioComponent->SetIntParameter("Material", 5);
				audioComponent->Play();
				break;
			default:
				break;
			}
		}
		colliding = false;
	}
}

