// Fill out your copyright notice in the Description page of Project Settings.


#include "HeadBobing.h"

UHeadBobing::UHeadBobing()
{
	bSingleInstance = true;

	OscillationDuration = 0.5f;
	OscillationBlendInTime = 0.1f;
	OscillationBlendOutTime = 0.2f;

	RotOscillation.Pitch.Amplitude = 0.5f;
	RotOscillation.Pitch.Frequency = 15.f;

	RotOscillation.Yaw.Frequency = 5.f;

	//RotOscillation.Roll.Amplitude = 1.f;
	//RotOscillation.Roll.Frequency = 9.f;

	LocOscillation.Z.Amplitude = 1.f;
	LocOscillation.Z.Frequency = 10.f;
}