// "Rooms With Secrets" by Natatem00. 2019


#include "PlayerHUD.h"

#include "Blueprint/UserWidget.h"
#include "UObject/ConstructorHelpers.h"

APlayerHUD::APlayerHUD()
{
	static ConstructorHelpers::FClassFinder<UUserWidget> widgetAsset(TEXT("/Game/Blueprints/UI/WBP_Player_HUD"));
	if (widgetAsset.Succeeded())
	{
		widgetClass = widgetAsset.Class;
	}
}

void APlayerHUD::BeginPlay()
{
	Super::BeginPlay();

	if (widgetClass)
	{
		widget = CreateWidget<UUserWidget>(GetWorld(), widgetClass);
		if (widget)
		{
			widget->AddToViewport();
		}
	}
}
