// "Rooms With Secrets" by Natatem00. 2019


#include "RWSGameInstance.h"

URWSGameInstance::URWSGameInstance()
{
	UE_LOG(LogTemp, Warning, TEXT("Game Instance Construct"));
}

void URWSGameInstance::Init()
{
	UE_LOG(LogTemp, Warning, TEXT("Game Instance Init"));
	graphicsManager = NewObject<UGraphicsManager>(this, TEXT("GraphicsManager"));
}

void URWSGameInstance::Host()
{

}



void URWSGameInstance::Join(const FString& address)
{
	GetEngine()->AddOnScreenDebugMessage(-1, 10.f, FColor::Green, "Joing "+ address);
}
