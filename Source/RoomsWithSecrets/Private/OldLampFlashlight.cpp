// Fill out your copyright notice in the Description page of Project Settings.


#include "OldLampFlashlight.h"

#include "Engine/Scene.h"
#include "Kismet/GameplayStatics.h"
#include "Components/AudioComponent.h"

#include "CEaseInOutInterpolation.h"
#include "CLinearInterpolation.h"

UOldLampFlashlight::UOldLampFlashlight()
{
	Intensity = 0;
	LightColor = FColor::FromHex("97856CFF");
	AttenuationRadius = 372.f;
	SourceRadius = 0;
	SoftSourceRadius = 0;
	SourceLength = 0;
	bUseTemperature = false;
	bAffectsWorld = true;
	CastShadows = false;
	IndirectLightingIntensity = 1;
	VolumetricScatteringIntensity = 1;

	IntensityUnits = ELightUnits::Unitless;
	bUseInverseSquaredFalloff = true;
	SpecularScale = 1;
	ShadowResolutionScale = 1;
	ShadowBias = 0.5f;
	ShadowSharpen = 0;
	ContactShadowLength = 0;
	
	CastStaticShadows = true;
	CastDynamicShadows = true;
	bAffectTranslucentLighting = true;

	bCastRaytracedShadow = false;
	bAffectReflection = false;

	audioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("Flashlight Audio"));
	audioComponent->bAutoActivate = false;

	SetIsReplicated(true);
}

bool UOldLampFlashlight::FadeIn(float deltaTime)
{
	FVector location = GetRelativeTransform().GetLocation();
	if ((int32)location.Z >= 0)
	{
		linearInterpolation.Reset();
		easeInOutInterpolation.Reset();
		return false;
	}
	FadeInInterpolation(location, deltaTime);

	return true;
}

void UOldLampFlashlight::FadeInInterpolation_Implementation(const FVector& location, float deltaTime)
{
	float zValue = easeInOutInterpolation.Interpolate(location.Z, 0, 2.f, deltaTime);
	SetIntensity(linearInterpolation.Interpolate(0.f, lightMaxIntansity, 0.2f, deltaTime));
	SetRelativeLocation(FVector(0, 0, zValue));
}

bool UOldLampFlashlight::FadeInInterpolation_Validate(const FVector& location, float deltaTime)
{
	return true;
}

bool UOldLampFlashlight::FadeOut(float deltaTime)
{
	FVector location = GetRelativeTransform().GetLocation();
	if ((int32)location.Z <= minLampZ)
	{
		linearInterpolation.Reset();
		easeInOutInterpolation.Reset();
		return false;
	}
	FadeOutInterpolation(location, deltaTime);

	return true;
}

void UOldLampFlashlight::FadeOutInterpolation_Implementation(const FVector& location, float deltaTime)
{
	float zValue = easeInOutInterpolation.Interpolate(location.Z, minLampZ, 2.f, deltaTime);
	SetIntensity(linearInterpolation.Interpolate(lightMaxIntansity, 0.f, 0.2f, deltaTime));
	SetRelativeLocation(FVector(0, 0, zValue));
}

bool UOldLampFlashlight::FadeOutInterpolation_Validate(const FVector& location, float deltaTime)
{
	return true;
}

void UOldLampFlashlight::SetupParentComponent(USceneComponent * inParent)
{
	if (inParent)
	{
		audioComponent->AttachTo(inParent);
		AttachTo(inParent);
		SetRelativeLocation(FVector(0, 0, minLampZ));
	}
}

void UOldLampFlashlight::PlaySound(bool isOn)
{
	if (soundCue)
	{
		audioComponent->SetIntParameter("OldLamp", isOn);
		audioComponent->Play();
	}
}
