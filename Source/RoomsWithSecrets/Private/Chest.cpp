// "Rooms With Secrets" by Natatem00. 2019


#include "Chest.h"

#include "Components/StaticMeshComponent.h"
#include "Components/AudioComponent.h"

// Sets default values
AChest::AChest()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	audioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	audioComponent->bAutoActivate = false;
}


void AChest::Rotate(const FRotator controllRotation)
{
	
}

void AChest::Use(AActor* useObject)
{
	if (IsOpen() && !animate)
	{
		animate = true;
		IInteractable::SetMesh(nullptr);
		audioComponent->Play();
	}
}

void AChest::PickUp(AActor* actorToPickup)
{

}

// Called when the game starts or when spawned
void AChest::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AChest::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (animate && mesh->GetRelativeTransform().GetRotation().Rotator().Roll > -40)
	{
		rotator += -DeltaTime * 0.4f;
		mesh->AddRelativeRotation(FRotator(0, 0, rotator));
		mesh->AddRelativeLocation(FVector(0, 0, 7 * DeltaTime));
	}
	else if (animate)
	{
		SetActorTickEnabled(false);
	}

}

