// "Rooms With Secrets" by Natatem00. 2019


#include "SpawnBag.h"

#include "Components/StaticMeshComponent.h"

#include "PickUp.h"
#include "FPSController.h"

// Sets default values
ASpawnBag::ASpawnBag()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	mesh->SetSimulatePhysics(false);
	RootComponent = mesh;

	IInteractable::SetMesh(mesh);
}

void ASpawnBag::Rotate(const FRotator controllRotation)
{
}

void ASpawnBag::Use(AActor* useObject)
{
	
}

void ASpawnBag::PickUp(AActor* actorToPickup)
{
	if (pickUpObjec)
	{
		auto pickUp = Cast<APickUp>(GetWorld()->SpawnActor(pickUpObjec));
		pickUp->SetStaticMesh(pickUpMesh);
		pickUp->SetSoundCue(pickUpSoundCue);
		Cast<AFPSController>(actorToPickup)->PickUpItem(pickUp);
	}
}

// Called when the game starts or when spawned
void ASpawnBag::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpawnBag::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

