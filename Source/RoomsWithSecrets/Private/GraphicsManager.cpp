// "Rooms With Secrets" by Natatem00. 2019


#include "GraphicsManager.h"

#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"


#define Resolution_Scale_Low TEXT("r.ScreenPercentage 25")
#define Resolution_Scale_LowMedium TEXT("r.ScreenPercentage 50")
#define Resolution_Scale_Medium TEXT("r.ScreenPercentage 75")
#define Resolution_Scale_High TEXT("r.ScreenPercentage 100")

#define View_Distance_Low TEXT("r.ViewDistanceScale 0.2")
#define View_Distance_LowMedium TEXT("r.ViewDistanceScale 0.5")
#define View_Distance_Medium TEXT("r.ViewDistanceScale 0.7")
#define View_Distance_High TEXT("r.ViewDistanceScale 1.0")

#define Antia_Aliasing_Low TEXT("r.PostProcessAAQuality 0")
#define Antia_Aliasing_LowMedium TEXT("r.PostProcessAAQuality 2")
#define Antia_Aliasing_Medium TEXT("r.PostProcessAAQuality 4")
#define Antia_Aliasing_High TEXT("r.PostProcessAAQuality 6")

#define Post_Process_Low TEXT("sg.PostProcessQuality 0")
#define Post_Process_LowMedium TEXT("sg.PostProcessQuality 1")
#define Post_Process_Medium TEXT("sg.PostProcessQuality 2")
#define Post_Process_High TEXT("sg.PostProcessQuality 3")

#define Shadow_Quality_Low TEXT("sg.ShadowQuality 0")
#define Shadow_Quality_LowMedium TEXT("sg.ShadowQuality 1")
#define Shadow_Quality_Medium TEXT("sg.ShadowQuality 2")
#define Shadow_Quality_High TEXT("sg.ShadowQuality 3")


UGraphicsManager::UGraphicsManager()
{
	if (FWindowsPlatformMisc::NumberOfCores() <= 2)
	{
		resolutionScale = EGraphicsSettings::MEDIUM;
	}
}

void UGraphicsManager::SetResolutionType(const EGraphicsSettings resolution, APlayerController* playerController, bool showLog)
{
	if (playerController)
	{
		resolutionScale = resolution;
		switch (resolutionScale)
		{
		case EGraphicsSettings::LOW:
			playerController->ConsoleCommand(Resolution_Scale_Low, showLog);
			break;
		case EGraphicsSettings::LOW_MEDIUM:
			playerController->ConsoleCommand(Resolution_Scale_LowMedium, showLog);
			break;
		case EGraphicsSettings::MEDIUM:
			playerController->ConsoleCommand(Resolution_Scale_Medium, showLog);
			break;
		case EGraphicsSettings::HIGH:
			playerController->ConsoleCommand(Resolution_Scale_High, showLog);
			break;
		}
	}
}

void UGraphicsManager::SetViewDistance(const EGraphicsSettings viewDistance, APlayerController* playerController, bool showLog)
{
	if (playerController)
	{
		this->viewDistance = viewDistance;
		switch (viewDistance)
		{
		case EGraphicsSettings::LOW:
			playerController->ConsoleCommand(View_Distance_Low, showLog);
			break;
		case EGraphicsSettings::LOW_MEDIUM:
			playerController->ConsoleCommand(View_Distance_LowMedium, showLog);
			break;
		case EGraphicsSettings::MEDIUM:
			playerController->ConsoleCommand(View_Distance_Medium, showLog);
			break;
		case EGraphicsSettings::HIGH:
			playerController->ConsoleCommand(View_Distance_High, showLog);
			break;
		}
	}
}

void UGraphicsManager::SetAntiAliasing(const EGraphicsSettings aliasing, APlayerController* playerController, bool showLog)
{
	if (playerController)
	{
		antiAliasing = aliasing;
		switch (antiAliasing)
		{
		case EGraphicsSettings::LOW:
			playerController->ConsoleCommand(Antia_Aliasing_Low, showLog);
			break;
		case EGraphicsSettings::LOW_MEDIUM:
			playerController->ConsoleCommand(Antia_Aliasing_LowMedium, showLog);
			break;
		case EGraphicsSettings::MEDIUM:
			playerController->ConsoleCommand(Antia_Aliasing_Medium, showLog);
			break;
		case EGraphicsSettings::HIGH:
			playerController->ConsoleCommand(Antia_Aliasing_High, showLog);
			break;
		}
	}
}

void UGraphicsManager::SetPostProcessQuality(const EGraphicsSettings quality, APlayerController* playerController, bool showLog)
{
	if (playerController)
	{
		postProcessQuality = quality;
		switch (postProcessQuality)
		{
		case EGraphicsSettings::LOW:
			playerController->ConsoleCommand(Post_Process_Low, showLog);
			break;
		case EGraphicsSettings::LOW_MEDIUM:
			playerController->ConsoleCommand(Post_Process_LowMedium, showLog);
			break;
		case EGraphicsSettings::MEDIUM:
			playerController->ConsoleCommand(Post_Process_Medium, showLog);
			break;
		case EGraphicsSettings::HIGH:
			playerController->ConsoleCommand(Post_Process_High, showLog);
			break;
		}
	}
}

void UGraphicsManager::SetShadowQuality(const EGraphicsSettings quality, APlayerController* playerController, bool showLog)
{
	if (playerController)
	{
		shadowQuality = quality;
		switch (shadowQuality)
		{
		case EGraphicsSettings::LOW:
			playerController->ConsoleCommand(Shadow_Quality_Low, showLog);
			break;
		case EGraphicsSettings::LOW_MEDIUM:
			playerController->ConsoleCommand(Shadow_Quality_LowMedium, showLog);
			break;
		case EGraphicsSettings::MEDIUM:
			playerController->ConsoleCommand(Shadow_Quality_Medium, showLog);
			break;
		case EGraphicsSettings::HIGH:
			playerController->ConsoleCommand(Shadow_Quality_High, showLog);
			break;
		}
	}
}
