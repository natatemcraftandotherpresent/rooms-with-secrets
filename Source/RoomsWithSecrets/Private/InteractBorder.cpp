// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractBorder.h"

#include "Components/BoxComponent.h"
#include "Components/AudioComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"

#include "Interactable.h"

// Sets default values
AInteractBorder::AInteractBorder()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	boxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Collider"));
	RootComponent = boxCollision;

	boxCollision->SetMobility(EComponentMobility::Static);

	audioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("Audio Component"));
	audioComponent->bAutoActivate = false;
}

// Called when the game starts or when spawned
void AInteractBorder::BeginPlay()
{
	Super::BeginPlay();
	
	boxCollision->OnComponentBeginOverlap.AddDynamic(this, &AInteractBorder::OnOverlapBegin);

}

// Called every frame
void AInteractBorder::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AInteractBorder::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	if (Cast<IInteractable>(OtherActor))
	{
		audioComponent->Play();
		if (particleSystem)
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), particleSystem, OtherActor->GetTransform().GetLocation());
		}
		OtherActor->SetActorLocationAndRotation(Cast<IInteractable>(OtherActor)->GetInitPosiiton(), Cast<IInteractable>(OtherActor)->GetInitRotation());
	}
}

