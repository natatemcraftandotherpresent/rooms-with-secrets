// Fill out your copyright notice in the Description page of Project Settings.


#include "FlashlightComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UFlashlightComponent::UFlashlightComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	SetIsReplicated(true);
}

void UFlashlightComponent::SetFlashlight(USceneComponent* flash)
{
	flashInt.SetInterface(Cast<IFlashLight>(flash));
	flashInt.SetObject(flash);
	if (flashInt)
	{
		flashInt->SetupParentComponent(this);
	}
}

bool UFlashlightComponent::OnFlashlight(float DeltaTime)
{
	if (flashInt)
	{
		return flashInt->FadeIn(DeltaTime);
	}
	return false;
}

bool UFlashlightComponent::OffFlashlight(float DeltaTime)
{
	if (flashInt)
	{
		return flashInt->FadeOut(DeltaTime);
	}
	return false;
}

void UFlashlightComponent::PlaySound(bool isFlashlightOn)
{
	if (flashInt)
	{
		flashInt->PlaySound(isFlashlightOn);
	}
}

void UFlashlightComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const 
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UFlashlightComponent, flashInt)
}


