// "Rooms With Secrets" by Natatem00. 2019

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Sound/SoundCue.h"

#include "Interactable.h"

#include "Lock.generated.h"

UCLASS()
class ROOMSWITHSECRETS_API ALock : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALock();

	virtual void Rotate(const FRotator controllRotation) override;

	virtual void Use(AActor* useObject = nullptr) override;
	virtual void PickUp(AActor* actorToPickup) override;

	UFUNCTION(BlueprintCallable, Category = "Lock")
	bool IsLocked() const { return keyToOpen != " "; }

	UFUNCTION(BlueprintCallable)
	void OpenLock()
	{
		//ALock::Use(GetKey(), nullptr);
	}
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "Lock")
	void SetMesh(UStaticMeshComponent* mesh) { this->mesh = mesh; }

	UFUNCTION(BlueprintCallable, Category = "Lock")
	void SetStaticLockerMeshComponent(UStaticMeshComponent* mesh)
	{
		this->lockMesh = mesh;
	}

	inline FString* GetKey() { return &keyToOpen; }

	AActor* GetChest() const { return chest; }

	UFUNCTION()
	void OnCompHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);
	
private:
	UPROPERTY(EditAnywhere)
	FString keyToOpen;

	UStaticMeshComponent* lockMesh;
	UStaticMeshComponent* mesh;

	UPROPERTY(EditAnywhere)
	UAudioComponent* audioComponent;

	AudioInteractable audioInteractable;

	UPROPERTY(EditAnywhere)
	AActor* chest;

	UPROPERTY(EditAnywhere)
	USoundCue* hitCue;
};
