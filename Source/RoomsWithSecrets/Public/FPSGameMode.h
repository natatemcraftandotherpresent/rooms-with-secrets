// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UObject/ConstructorHelpers.h"
#include "GameFramework/HUD.h"

#include "PlayerHUD.h"

#include "FPSGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ROOMSWITHSECRETS_API AFPSGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFPSGameMode()
	{
		static ConstructorHelpers::FClassFinder<APawn> playerPawn(TEXT("/Game/Blueprints/Actors/BP_FPSPlayer"));
		if (playerPawn.Class != NULL)
		{
			DefaultPawnClass = playerPawn.Class;
		}
		static ConstructorHelpers::FClassFinder<AController> playerController(TEXT("/Game/Blueprints/Controllers/BPC_MagicVillageController"));
		if (playerController.Class != NULL)
		{
			PlayerControllerClass = playerController.Class;
		}
		HUDClass = APlayerHUD::StaticClass();
	}
	
};
