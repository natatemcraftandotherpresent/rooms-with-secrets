// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Sound/SoundCue.h"
#include "FlashLight.generated.h"


// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UFlashLight : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class ROOMSWITHSECRETS_API IFlashLight
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual bool FadeIn(float deltaTime) = 0;

	virtual bool FadeOut(float deltaTime) = 0;

	virtual void SetupParentComponent(USceneComponent* inParent) = 0;

	virtual void PlaySound(bool isOn) = 0;
};
