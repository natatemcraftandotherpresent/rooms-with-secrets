// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class UAudioComponent;
class UStaticMeshComponent;
struct ROOMSWITHSECRETS_API ChangeMesh
{
public:

	void Outline(bool show);

	void SetMesh(UStaticMeshComponent* meshComponent) 
	{ 
		if (!meshComponent) Outline(false);
		this->meshComponent = meshComponent; 
	}

private:
	UStaticMeshComponent* meshComponent;
};

struct ROOMSWITHSECRETS_API AudioInteractable
{
public:
	void SetAudioComponent(UAudioComponent* audioComponent);
	void SetColliding(bool collided) { colliding = collided; }
	void OnCompHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComponent, FVector NormalImpulse, const FHitResult & Hit);
private:
	UAudioComponent* audioComponent;
	bool colliding;
};

