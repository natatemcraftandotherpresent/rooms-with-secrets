// "Rooms With Secrets" by Natatem00. 2019

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "GameFramework/Character.h"

#include "Constans.h"

#include "Interactable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInteractable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class ROOMSWITHSECRETS_API IInteractable
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual void Rotate(const FRotator controllRotation) = 0;

	virtual void Use(AActor* useObject = nullptr) = 0;
	virtual void PickUp(AActor* actorToPickup) = 0;

	void Outline(bool show);

	void SetInitPosition(FVector position);
	FVector GetInitPosiiton() const;

	void SetInitRotation(FRotator rotation);
	FRotator GetInitRotation() const;

	void SetMesh(UStaticMeshComponent* mesh);
private:
	FVector initPosition;
	FRotator initRotation;

	ChangeMesh changeMesh;

};
