// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class ROOMSWITHSECRETS_API CEaseInOutInterpolation
{
public:

	//static bool Setup(float from, float to, float time);
	//static float Interpolate(float deltaTime);
	float Interpolate(float from, float to, float time, float deltaTime);
	void Reset();

private:
	float value = 0;
	float target;
	float remainingTime;
	float totalTime;
	float speed;
	float acceleration;
};

