// "Rooms With Secrets" by Natatem00. 2019

#pragma once

#include "CoreMinimal.h"

#include "Sound/SoundCue.h"

#include "PickUp.h"

#include "FillIn.generated.h"

USTRUCT()
struct FKeys
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY(EditAnywhere) FString key1;
	UPROPERTY(EditAnywhere) int numKey1;
	UPROPERTY(EditAnywhere) FString key2;
	UPROPERTY(EditAnywhere) int numKey2;
	UPROPERTY(EditAnywhere) FString key3;
	UPROPERTY(EditAnywhere) int numKey3;
	UPROPERTY(EditAnywhere) FString key4;
	UPROPERTY(EditAnywhere) int numKey4;
};

UCLASS()
class ROOMSWITHSECRETS_API AFillIn : public APickUp
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFillIn();

	virtual void Use(AActor* useObject = nullptr) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	bool ready = false;

	UPROPERTY(EditAnywhere)
	FKeys kes;
	UPROPERTY(EditAnywhere)
	AActor* soupMesh;
	UPROPERTY(EditAnywhere)
	USoundCue* hitCue;
};
