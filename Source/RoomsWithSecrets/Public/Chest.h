// "Rooms With Secrets" by Natatem00. 2019

#pragma once

#include "CoreMinimal.h"

#include "Interactable.h"
#include "Locker.h"

#include "Chest.generated.h"

UCLASS()
class ROOMSWITHSECRETS_API AChest : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AChest();

	virtual void Rotate(const FRotator controllRotation) override;

	virtual void Use(AActor* useObject = nullptr) override;
	virtual void PickUp(AActor* actorToPickup) override;

	inline void Open(bool open) { this->open = open; }

	inline bool IsOpen() const { return open; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "Chest Component")
	void SetStaticMeshComponent(UStaticMeshComponent* mesh) 
	{ 
		this->mesh = mesh; 
		IInteractable::SetMesh(this->mesh);
	}
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UStaticMeshComponent* mesh;

	bool animate = false;

	float rotator = 0;

	UPROPERTY(EditAnywhere)
	UAudioComponent* audioComponent;

	bool open;
};
