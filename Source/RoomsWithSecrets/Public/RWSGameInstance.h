// "Rooms With Secrets" by Natatem00. 2019

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"

#include "GraphicsManager.h"

#include "RWSGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class ROOMSWITHSECRETS_API URWSGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	URWSGameInstance();

	void Init() override;

	UFUNCTION(Exec)
	void Host();
	UFUNCTION(Exec)
	void Join(const FString& address);

	UFUNCTION(BlueprintCallable, Category = "Managers")
	UGraphicsManager* GetGraphicsManager() { return graphicsManager; }

private:
	UPROPERTY()
	UGraphicsManager* graphicsManager;
};
