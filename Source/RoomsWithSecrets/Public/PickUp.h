// "Rooms With Secrets" by Natatem00. 2019

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Sound/SoundCue.h"

#include "Interactable.h"
#include "Constans.h"

#include "PickUp.generated.h"

class ACharacter;
class UCameraComponent;
UCLASS()
class ROOMSWITHSECRETS_API APickUp : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickUp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnCompHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//void Pickup(const FVector& cameraForwardVector);

	virtual void Rotate(const FRotator controllRotation) override;

	virtual void Use(AActor* useObject = nullptr) override;
	virtual void PickUp(AActor* actorToPickup) override;

	void TogglePickUp();

	FString GetKey() const { return keyName; }

	inline void SetStaticMesh(UStaticMesh* meshStatic) { mesh->SetStaticMesh(meshStatic); }
	void SetSoundCue(USoundCue* audioCue);

	inline UAudioComponent* GetAudioComponent() const { return audioComponent; }
private:
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* mesh;

	USceneComponent* holdingComponent;

	UPROPERTY(EditAnywhere)
	UAudioComponent* audioComponent;

	bool isHolding;
	bool useGravity;
	bool colliding = false;

	UPROPERTY(EditAnywhere)
	FString keyName;

	AudioInteractable audioInteractable;
};
