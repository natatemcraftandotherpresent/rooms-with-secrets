// "Rooms With Secrets" by Natatem00. 2019

#pragma once

#include "CoreMinimal.h"


/**
 * 
 */
template<class SINGLETON>
class ROOMSWITHSECRETS_API USingleton
{
public:
	USingleton()
	{
		singleton = (SINGLETON*)(this);
	}
	virtual ~USingleton()
	{
		singleton = nullptr;
	}

	static SINGLETON& GetSingleton() { return *singleton; }
	static SINGLETON* GetSingletonPtr()  { return singleton; }
private:
	static SINGLETON* singleton;
};

template<class SINGLETON>
SINGLETON* USingleton<SINGLETON>::singleton = 0;

