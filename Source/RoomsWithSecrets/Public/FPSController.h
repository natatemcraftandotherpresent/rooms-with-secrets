// "Rooms With Secrets" by Natatem00. 2019

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraShake.h"

#include "CEaseInOutInterpolation.h"
#include "PickUp.h"

#include "FPSController.generated.h"

class ACharacter;
class UCameraComponent;
class USpringArmComponent;
class UFlashlightComponent;

USTRUCT()
struct FControlSettings{
	GENERATED_BODY()

public:
	float mouseSensivity = 0.1f;
};

UCLASS()
class ROOMSWITHSECRETS_API AFPSController : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AFPSController();

	UFUNCTION(BlueprintCallable, Category = "Player Settngs")
	void SetMouseSensivity(float value) { controlSettings.mouseSensivity = value; }
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "Player Components")
	void SetSpringArm(USpringArmComponent* spring);

	UFUNCTION(BlueprintCallable, Category = "Player Components")
	void SetCamera(UCameraComponent* camera);

	UFUNCTION(BlueprintCallable, Category = "Player")
	void SetCameraShake(TSubclassOf<UCameraShake> cameraShake) { this->cameraShake = cameraShake; }

	UFUNCTION(BlueprintCallable, Category = "Player")
	EPhysicalSurface CheckStep();

	UFUNCTION(BlueprintCallable, Category = "Player Components")
	void SetFlashlightComponent(UFlashlightComponent* flashlightComponent) { this->flashlightComponent = flashlightComponent; }

	UFUNCTION(BlueprintCallable, Category = "Player")
	float GetStepTime() const { return stepTime; }

	UFUNCTION(BlueprintCallable, Category = "Player")
	void SetCanMove(bool move);

	UFUNCTION(BlueprintPure, Category = "Player")
	bool GetCanMove() { return canMove; }
protected:
	UPROPERTY(BlueprintReadOnly, Category = "Player")
	bool flashlightOn;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(Server, WithValidation, Reliable)
	void ToggleFlashlight(float DeltaTime);

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	void PickUpItem(IInteractable* item);
private:
	void MoveRight(float value);
	void MoveForward(float value);
	void Crouch(float DeltaTime);

	void OnCrouch();
	void OnFlashlight();
	void OnAction();
	void OnInspect();
	void OnInspectReleased();
	void OnCancel();

	void ToggleItemPickup();
	void ToggleMove();

	void CheckInteractableObject();
	void InspectObject(float DeltaTime);

	void YawRotation(float rotation);
	void PitchRotation(float rotation);

private:
	bool crouch;
	bool isHolding;
	bool isInspecting;
	bool canMove = true;

	USpringArmComponent* springArm;
	UCameraComponent* cameraComponent;
	TSubclassOf<UCameraShake> cameraShake;

	FVector springOriginalPosition;
	FVector crouchPosition;

	FVector holdingComp;
	FRotator lastRotation;

	FHitResult hitResult;

	float moveSpeedFactor = 0.7f;
	float stepTime = 0.4f;
	float pitchMax;
	float pitchMin;
	float interpolFactor = 0;

	CEaseInOutInterpolation easeInOutInterpolation;
	UPROPERTY(Replicated)
	UFlashlightComponent* flashlightComponent;

	UPROPERTY(EditAnywhere)
	USceneComponent* holdingComponent;
	IInteractable* currentPickupObj;

	IInteractable* lastHitedObject;

	UPROPERTY(EditAnywhere)
	UAudioComponent* audioComponent;

	FControlSettings controlSettings;
};
