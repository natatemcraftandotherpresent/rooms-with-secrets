// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "FlashLight.h"
#include "FlashlightComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ROOMSWITHSECRETS_API UFlashlightComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFlashlightComponent();

protected:

public:	
	UFUNCTION(BlueprintCallable, Category = "Flashlight")
	void SetFlashlight(USceneComponent* flash);

	bool OnFlashlight(float DeltaTime);
	bool OffFlashlight(float DeltaTime);

	void PlaySound(bool isFlashlightOn);
private:
	UPROPERTY(Replicated)
	TScriptInterface<IFlashLight> flashInt;
};
