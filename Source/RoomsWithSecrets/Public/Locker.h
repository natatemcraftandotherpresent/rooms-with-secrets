// "Rooms With Secrets" by Natatem00. 2019

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Locker.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class ULocker : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class ROOMSWITHSECRETS_API ILocker
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
private:
	
};
