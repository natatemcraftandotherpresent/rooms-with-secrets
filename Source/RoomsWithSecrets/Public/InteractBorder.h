// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractBorder.generated.h"

class UBoxComponent;
UCLASS()
class ROOMSWITHSECRETS_API AInteractBorder : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteractBorder();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
						int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	

private:
	UPROPERTY(EditAnywhere)
	UBoxComponent* boxCollision;
	UPROPERTY(EditAnywhere)
	UAudioComponent* audioComponent;
	UPROPERTY(EditAnywhere)
	UParticleSystem* particleSystem;
};
