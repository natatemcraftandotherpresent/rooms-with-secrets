// "Rooms With Secrets" by Natatem00. 2019

#pragma once

#include "CoreMinimal.h"

#include "Singleton.h"

#include "GraphicsManager.generated.h"

/**
 * 
 */

UENUM(BlueprintType)
enum class EGraphicsSettings : uint8
{
	LOW,
	LOW_MEDIUM,
	MEDIUM,
	HIGH
};

UCLASS(BlueprintType)
class ROOMSWITHSECRETS_API UGraphicsManager : public UObject
{
	GENERATED_BODY()
public:
	UGraphicsManager();
	virtual ~UGraphicsManager() = default;

	UFUNCTION(BlueprintCallable, Category = "Graphics|Resolution")
	void SetResolutionType(const EGraphicsSettings resolution, APlayerController* playerController, bool showLog = false);
	UFUNCTION(BlueprintCallable, Category = "Graphics|ViewDistance")
	void SetViewDistance(const EGraphicsSettings viewDistance, APlayerController* playerController, bool showLog = false);
	UFUNCTION(BlueprintCallable, Category = "Graphics|Anti-Aliasing")
	void SetAntiAliasing(const EGraphicsSettings aliasing, APlayerController* playerController, bool showLog = false);
	UFUNCTION(BlueprintCallable, Category = "Graphics|PostProcess")
	void SetPostProcessQuality(const EGraphicsSettings quality, APlayerController* playerController, bool showLog = false);
	UFUNCTION(BlueprintCallable, Category = "Graphics|Shadows")
	void SetShadowQuality(const EGraphicsSettings quality, APlayerController* playerController, bool showLog = false);

	UFUNCTION(BlueprintPure, Category = "Graphics|Resolution")
	EGraphicsSettings GetResolutionScale() const { return resolutionScale; }
	UFUNCTION(BlueprintPure, Category = "Graphics|ViewDistance")
	EGraphicsSettings GetViewDistance() const { return viewDistance; }
	UFUNCTION(BlueprintPure, Category = "Graphics|PostProcess")
	EGraphicsSettings GetPostProcess() const { return postProcessQuality; }
private:
	EGraphicsSettings resolutionScale = EGraphicsSettings::HIGH;
	EGraphicsSettings viewDistance = EGraphicsSettings::HIGH;
	EGraphicsSettings antiAliasing = EGraphicsSettings::HIGH;
	EGraphicsSettings postProcessQuality = EGraphicsSettings::HIGH;
	EGraphicsSettings shadowQuality = EGraphicsSettings::HIGH;
};
