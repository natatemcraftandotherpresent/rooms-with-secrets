// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RoomsWithSecretsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ROOMSWITHSECRETS_API ARoomsWithSecretsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
