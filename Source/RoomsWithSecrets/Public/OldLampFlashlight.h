// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/PointLightComponent.h"
#include "FlashLight.h"
#include "CEaseInOutInterpolation.h"
#include "CLinearInterpolation.h"
#include "Components/AudioComponent.h"
#include "OldLampFlashlight.generated.h"

/**
 * 
 */
class CLinearInterpolation;
UCLASS()
class ROOMSWITHSECRETS_API UOldLampFlashlight : public UPointLightComponent, public IFlashLight
{
	GENERATED_BODY()
public:
	UOldLampFlashlight();

	virtual bool FadeIn(float deltaTime) override;

	UFUNCTION(NetMulticast, WithValidation, Reliable)
	void FadeInInterpolation(const FVector& location, float deltaTime);

	virtual bool FadeOut(float deltaTime) override;

	UFUNCTION(NetMulticast, WithValidation, Reliable)
	void FadeOutInterpolation(const FVector& location, float deltaTime);

	virtual void SetupParentComponent(USceneComponent* inParent) override;

	virtual void PlaySound(bool isOn) override;

protected:

	UFUNCTION(BlueprintCallable, Category = "Flashlight")
	void SetSoundCue(USoundCue* soundCue) 
	{ 
		if (soundCue)
		{
			this->soundCue = soundCue;
			audioComponent->SetSound(this->soundCue);
		}
	}

private:

	float lightMaxIntansity = 20000.0f;
	float minLampZ = -40.f;
	float maxLampZ = 0.f;

	CEaseInOutInterpolation easeInOutInterpolation;
	CLinearInterpolation linearInterpolation;

	USoundCue* soundCue;
	UAudioComponent* audioComponent;
};
