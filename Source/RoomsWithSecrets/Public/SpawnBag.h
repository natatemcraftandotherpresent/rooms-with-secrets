// "Rooms With Secrets" by Natatem00. 2019

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Sound/SoundCue.h"

#include "Interactable.h"

#include "SpawnBag.generated.h"

UCLASS()
class ROOMSWITHSECRETS_API ASpawnBag : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnBag();

	virtual void Rotate(const FRotator controllRotation) override;

	virtual void Use(AActor* useObject = nullptr) override;
	virtual void PickUp(AActor* actorToPickup) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(EditAnywhere)
	TSubclassOf<class APickUp> pickUpObjec;
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* mesh;

	UPROPERTY(EditAnywhere)
	UStaticMesh* pickUpMesh;
	UPROPERTY(EditAnywhere)
	USoundCue* pickUpSoundCue;
};
