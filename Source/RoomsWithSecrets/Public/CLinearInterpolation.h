// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class ROOMSWITHSECRETS_API CLinearInterpolation
{
public:
	float Interpolate(float from, float to, float time, float deltaTime);
	void Reset();
private:
	float value = 0;
	float step = 0;
	float remainingTime = 0;
};
