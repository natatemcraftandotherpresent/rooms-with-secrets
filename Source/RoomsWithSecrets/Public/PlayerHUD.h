// "Rooms With Secrets" by Natatem00. 2019

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "PlayerHUD.generated.h"

/**
 * 
 */
UCLASS()
class ROOMSWITHSECRETS_API APlayerHUD : public AHUD
{
	GENERATED_BODY()

public:
	APlayerHUD();

private:
	virtual void BeginPlay() override;

private:
	TSubclassOf<UUserWidget> widgetClass;

	class UUserWidget* widget;
};
